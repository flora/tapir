import requests

from .utility import _get, _put, _post, _delete


def departures(session: requests.Session, station_id: int):
    """Get departures from a station

    Args:
        session (requests.Session): _description_
        station_id (int): _description_
    """
    return _get(session, f"/station/{station_id}/departures", {})
