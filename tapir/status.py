import requests

from .utility import _get, _put, _post, _delete


def active(session: requests.Session):
    """Returns all currently active statuses that are visible to the (un)authenticated user
    """
    return _get(session, "/statuses", {})


def get(session: requests.Session, status_id: int):
    """Returns a single status Object, if user is authorized to see it
    """
    return _get(session, f"/status/{status_id}", {})


def update(session: requests.Session, status_id: int, status: dict):
    """Updates a single status Object, if user is authorized to
    """
    return _put(session, f"/status/{status_id}", status)


def delete(session: requests.Session, status_id: int):
    """Deletes a single status Object, if user is authorized to
    """
    return _delete(session, f"/status/{status_id}", {})


def get_tags_of(session: requests.Session, status_id: int):
    """Returns a collection of all visible tags for the given status, if user is authorized
    """
    return _get(session, f"/status/{status_id}/tags", {})


def add_tag_to(session: requests.Session, status_id: int, tag: dict):
    """Creates a single StatusTag Object, if user is authorized to.

    The key of a tag is free * text. You can choose it as you need it. However, please use a namespace for tags * (namespace:xxx) that only affect your own application.
    """
    return _post(session, f"/status/{status_id}/tags", tag)


def get_all_tags_of(session: requests.Session, status_ids: list[int]):
    """Returns a collection of all visible tags for the given statuses, if user is authorized
    """
    return _get(session, f"/statuses/{','.join(status_ids)}/tags", {})


def update_tag_of(session: requests.Session, status_id: int, tag_key: str, tag: dict):
    """Updates a single StatusTag Object, if user is authorized to
    """
    return _put(session, f"/status/{status_id}/tags/{tag_key}", tag)


def delete_tag_from(session: requests.Session, status_id: int, tag_key: str):
    """Deletes a single StatusTag Object, if user is authorized to
    """
    return _delete(session, f"/status/{status_id}/tags/{tag_key}", {})
