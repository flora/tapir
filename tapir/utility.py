import os
from typing import Optional

import certifi
import dotenv
import requests
import url_normalize as urlencoder

from . import exceptions


def uri_encode(uri: str) -> str:
    return urlencoder.url_normalize(uri)


def open_session(token: Optional[str] = None):
    session = requests.Session()

    if token is None:
        token = os.getenv("traewelling_token")

    if token is not None:
        session.headers = {"Authorization": "Bearer " + token}
    # session.cert = certifi.where()
    session.verify = False

    return session


def _build_url(suffix: str) -> str:
    try:
        return uri_encode(os.getenv("traewelling_url") + suffix)
    except TypeError:
        if os.getenv("traewelling_url") is None:
            raise exceptions.NoURLSet
        else:
            raise TypeError


def add_page_query(page: Optional[int]):
    if page is None:
        return ""
    else:
        return f'?page={page}'


def _get(session: requests.Session, suffix: str, params: dict) -> dict:
    response = session.get(_build_url(suffix), params=params)

    if response.ok:
        return response.json()
    response.raise_for_status()


def _put(session: requests.Session, suffix: str, params: dict) -> dict:
    response = session.put(_build_url(suffix), params=params)

    if response.ok:
        return response.json()
    response.raise_for_status()


def _post(session: requests.Session, suffix: str, params: dict) -> dict:
    response = session.post(_build_url(suffix), params=params)

    if response.ok:
        return response.json()
    response.raise_for_status()


def _delete(session: requests.Session, suffix: str, params: dict) -> dict:
    response = session.delete(_build_url(suffix), params=params)

    if response.ok:
        return response.json()
    response.raise_for_status()


def set_url(new_url: str):
    os.putenv("traewelling_url", new_url)


def load_env():
    dotenv.load_dotenv(override=True)
