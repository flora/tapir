import os

import requests

from . import utility


def logout(session: requests.Session):
    """Logout & invalidate current bearer token
    """
    return utility._post(session, "/auth/logout", {})


def user(session: requests.Session):
    """Get all profile information about the authenticated user
    """
    return utility._get(session, "/auth/user", {})


def refresh(session: requests.Session):
    """This request issues a new Bearer-Token with a new expiration date while also revoking the old * token.
    """
    response = utility._post(session, "/auth/refresh", {})["data"]
    session.headers = {**session.headers, **
                       {"Authorization": "Bearer " + response["token"]}}
    os.putenv("traewelling_token",  response["token"])

    return response
