class NoURLSet(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class NoTokenFound(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
