from typing import Optional

import requests

from . import utility


def statuses_of(session: requests.Session, username: str, page: Optional[int] = None):
    """Returns paginated statuses of a single user specified by the username

    Args:
        session (requests.Session): _description_
        username (str): _description_
        page (Optional[int], optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """

    return utility._get(session, f"/user/{username}/statuses{utility.add_page_query(page)}", {})


def get(session: requests.Session, username: str, page: Optional[int] = None):
    """Returns general information, metadata and statistics for a user

    Args:
        session (requests.Session): _description_
        username (str): _description_
        page (Optional[int], optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """

    return utility._get(session, f"/user/{username}{utility.add_page_query(page)}", {})


def search(session: requests.Session, username: str, page: Optional[int] = None):
    """Returns paginated statuses of a single user specified by the username

    Args:
        session (requests.Session): _description_
        username (str): _description_
        page (Optional[int], optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """

    return utility._get(session, f"/user/search/{username}{utility.add_page_query(page)}", {})
