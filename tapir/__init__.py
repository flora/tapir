import logging

from . import auth, utility, station, user, status

try:
    utility.load_env()
except:
    logging.warn("No .env was found")
